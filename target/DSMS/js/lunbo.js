var oBox = document.getElementById('box');
var oPicBox = document.getElementById('pic');
var oDotBox = document.getElementById('dot');
var oPics = oPicBox.getElementsByTagName('li');
var oDots = oDotBox.getElementsByTagName('li');
//自动播放
//需要得到一个0-3不断发生变化的数字
var num = 0;
var len = oPics.length;
var timer = null;//储存定时器
//外包的autoPlay函数是为了之后调用 鼠标离开图片继续播放，避免函数重复
function autoPlay(){
    timer = setInterval(function () {
        num++;
        if(num>4){
            num=0;
        }
        // if(num>len-1){
        //     num=0;
        // }
        //去掉所有li的类名，因为在使用一个循环之后每个li都有active之后效果将停止
        for(var i=0;i<len;i++){
            oPics[i].className = '';
            oDots[i].className = '';
        }
        //console.log(num);
        oPics[num].className = 'active';
        oDots[num].className = 'active';
    },1500);
}
//调用
autoPlay();
//鼠标移动到图片上范围停止自动播放,之后不再进行轮播
oBox.onmouseover = function () {
    clearInterval(timer);
}
//鼠标离开图片继续播放
oBox.onmouseout = function () {
    autoPlay();
}
//鼠标点击圆角控制
for(var i = 0;i<len;i++){

    //在dot里的li添加索引号的作用，js没法读取li下的index
    oDots[i].index = i;
    oDots[i].onclick = function () {
        //清除每次使用
        for(var i=0;i<len;i++){
            //清空类名，否则点击一个循环之后重新点击没有变化
            oPics[i].className = '';
            oDots[i].className = '';
        }
        this.className = 'active';
        oPics[this.index].className = 'active';
        //当前圆角的索引值
        //this.index;
        //更新num的值，否则是随机播放
        num = this.index;
    }
}