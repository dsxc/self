package com.dsx.dsms.service;

import com.dsx.dsms.pojo.ExamInfo;
import com.dsx.dsms.pojo.Student;
import com.dsx.dsms.util.Page;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author dsx
 */
public interface StudentService {

    /**
     *
     * 返回学生列表
     * @param page 分页
     * @return 返回学生列表
     */
    List<Student> list(Page page);

    /**
     * 根据进度返回学员
     * @param page 分页
     * @param schedule 学员进度
     * @return 学员
     */
    List<Student> listBySchedule(Page page, int schedule);

    /**
     * 返回学生列表
     * @param page 分页
     * @param schedule 进度
     * @param key 搜索关键字
     * @return 学生
     */
    List<Student> listStudentBySearch(Page page, String schedule, String key);



    /**
     * 添加学生
     * @param student 学生
     */
    void add(Student student);

    /**
     * 根据id 删除学生
     * @param id 学生id
     */
    void delete(int id);

    /**
     * 根据进度返回学生人数
     * @param schedule 进度
     * @return 学生人数
     */
    int total(int schedule);

    /**
     * 返回下一次考试的学员  如果subject2 不为0 就返回考科目二的 否则就是科目三的
     * @param subject2 科目二教练id
     * @param subject3 科目三教练id
     * @param date  当前日期
     * @param page 分页
     * @return 学院列表
     */
    List<Student> listStudentByNextTimeExam(int subject2, int subject3,
                                            Date date, Page page);

    /**
     * 返回下一次考试的学员 人数 如果subject2 不为0 就返回考科目二的 否则就是科目三的
     * @param subject2 科目二教练id
     * @param subject3 科目三教练id
     * @param date  当前日期
     * @return 学院列表
     */
    int totalStudentByNextTimeExam(int subject2, int subject3, Date date);


    /**
     * 返回预约的学生集合
     * @param id 教练id
     * @param date 日期
     * @param period 时间段
     * @return 预约的学生
     */
    List<Student> listStudentByReservationPeriod(int id, String date, int period);

    /**
     * 返回学员列表
     * @param id 教练id
     * @param type 科目二 或 科目三
     * @param page 分页
     * @return  学员列表
     */
    List<Student> listStudentByCid(int id, String type,Page page);

    /**
     * 此教练的总学员数
     * @param cid 教练id
     * @param type 教练类型
     * @return 此教练的总学员数
     */
    int totalStudentByCid(int cid, String type);





    /**
     * 获取上一次考试的学生列表
     * @param subject2  科目二教练id
     * @param subject3  科目三教练id
     * @param date  考试日期
     * @param page 分页
     * @return  学生列表
     */
    List<Student> listStudentByLastTimeExamInfo(int subject2, int subject3, Date date, Page page);
/**
     * 获取上一次考试的学生数
     * @param subject2  科目二教练id
     * @param subject3  科目三教练id
     * @param date  考试日期
     * @return  学生列表
     */
    int totalStudentByLastTimeExamInfo(int subject2, int subject3, Date date);

    /**
     * 更新
     * @param student 学生
     */
    void update(Student student);


    /**
     * 根据id 返回学生
     * @param id 学生id
     * @return 返回学生
     */
    Student getStudentById(int id);
}
