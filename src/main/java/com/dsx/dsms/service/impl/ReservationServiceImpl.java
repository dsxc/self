package com.dsx.dsms.service.impl;

import com.dsx.dsms.mapper.ReservationMapper;
import com.dsx.dsms.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    ReservationMapper mapper;
    @Override
    public void updateReservation(String period, int num, int cid, String date) {

        System.out.println("maper------------> " + num);
        mapper.updateReservation(period,num,cid,date);
    }

    @Override
    public void insertReservation(int sid, int cid, String date, int period, int subject) {
        mapper.insertReservation(sid,cid,date,period,subject);
    }

    @Override
    public void deleteReservation(int sid, String date, int period) {
        mapper.deleteReservation(sid,date,period);
    }

    @Override
    public int isReservation(int sid, String date, int period) {
        return  mapper.isReservation(sid,date,period);
    }

    @Override
    public int isLeave(int sid, String date, int period) {
        return mapper.isLeave(sid,date,period);
    }

    @Override
    public int ReservationTime(int sid, String date) {
        return mapper.ReservationTime(sid,date);
    }

    @Override
    public void resetTime(int cid, String date, int[] time) {
        mapper.resetTime(cid, date, time);
        for(int i=0; i<8; i++){
            System.out.println(time[i]);
        }

    }
}
