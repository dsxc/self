package com.dsx.dsms.service.impl;

import com.dsx.dsms.mapper.UserMapper;
import com.dsx.dsms.pojo.Student;
import com.dsx.dsms.pojo.User;
import com.dsx.dsms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;


    @Override
    public User getUserByType(String username, String password, int type) {
        User user = null;
        switch (type){
            case 0:
                user = userMapper.getManager(username,password);
                break;
            case 1:
                user = userMapper.getCoach(username,password);
                break;
            case 2:
                user = userMapper.getStudent(username,password);
                break;
            default: break;
        }
        return user;
    }

    @Override
    public void upload(User user) {
        userMapper.upload(user);
    }

    @Override
    public void delete(int id) {
        userMapper.delete(id);
    }

    @Override
    public User getUserByFid(int fid, int type) {
        return userMapper.getUserByFid(fid,type);
    }

    @Override
    public int getUserByUserName(String username) {
        return userMapper.getUserByUserName(username);
    }

    @Override
    public int insertStudent(Student student) {
           return userMapper.insertStudent(student);
    }

    @Override
    public void insertUser(String username, String password, int fid, String sex, String pic, int type) {
        userMapper.insertUser(username, password, fid, sex, pic, type);
    }
}
