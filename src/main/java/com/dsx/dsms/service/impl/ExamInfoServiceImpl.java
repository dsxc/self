package com.dsx.dsms.service.impl;

import com.dsx.dsms.mapper.ExamInfoMapper;
import com.dsx.dsms.pojo.ExamInfo;
import com.dsx.dsms.service.ExamInfoService;
import com.dsx.dsms.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;

/**
 * @author dsx
 */
@Service
public class ExamInfoServiceImpl implements ExamInfoService {


    @Autowired
    ExamInfoMapper examInfoMapper;

    @Override
    public void add(ExamInfo examInfo) {
        examInfoMapper.add(examInfo);
    }

    @Override
    public void delete(int id) {
        examInfoMapper.delete(id);
    }

    @Override
    public void update(ExamInfo examInfo) {
        examInfoMapper.update(examInfo);
    }

    @Override
    public void updateStudentExamStatus(int eid, int status) {
        examInfoMapper.updateStudentExamStatus(eid, status);
    }

    @Override
    public List<ExamInfo> list(Page page) {
        return examInfoMapper.list(page);
    }

    @Override
    public ExamInfo getExamInfoById(int id) {
        return examInfoMapper.getExamInfoById(id);
    }

    @Override
    public int total() {
        return examInfoMapper.total();
    }

    @Override
    public ExamInfo getRecentlyExam(int id) {
        return examInfoMapper.getRecentlyExam(id);
    }

    @Override
    public List<ExamInfo> listExamInfoByDate(Date date) {
        return examInfoMapper.listExamInfoByDate(date);
    }

    @Override
    public void insertStudentExamInfo(ExamInfo ef, int id) {
        examInfoMapper.insertStudentExamInfo(ef,id);
    }

    @Override
    public void cancelExam(Date date, int sid) {
        examInfoMapper.cancelExam(date,sid);
    }

    @Override
    public ExamInfo getExamInfoByNextTime(int sid, Date date) {
        return examInfoMapper.getExamInfoByNextTime(sid,date);
    }

    @Override
    public ExamInfo lastTimeExamInfo(Date date, int type) {
        return examInfoMapper.lastTimeExamInfo(date, type);
    }
}
