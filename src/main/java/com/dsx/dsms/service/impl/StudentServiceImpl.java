package com.dsx.dsms.service.impl;

import com.dsx.dsms.mapper.StudentMapper;
import com.dsx.dsms.pojo.ExamInfo;
import com.dsx.dsms.pojo.Student;
import com.dsx.dsms.service.StudentService;
import com.dsx.dsms.util.Page;
import com.dsx.dsms.util.Transform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
/**
 * @author dsx
 */
@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    StudentMapper studentMapper;
    @Override
    public List<Student> list(Page page) {
        return studentMapper.list(page);
    }

    @Override
    public List<Student> listBySchedule(Page page, int schedule) {
        String temp = new Transform().change(schedule);
        return studentMapper.listBySchedule(page,temp);
    }

    @Override
    public List<Student> listStudentBySearch(Page page, String schedule, String key) {
        return studentMapper.listStudentBySearch(page, schedule, key);
    }

    @Override
    public void add(Student student) {
        studentMapper.add(student);
    }


    @Override
    public void delete(int id) {
        studentMapper.delete(id);
    }

    @Override
    public int total(int schedule) {
        String temp = new Transform().change(schedule);
        return studentMapper.total(temp);
    }

    @Override
    public List<Student> listStudentByNextTimeExam(int subject2, int subject3, Date date, Page page) {
        return studentMapper.listStudentByNextTimeExam(subject2,subject3,date,page);
    }

    @Override
    public int totalStudentByNextTimeExam(int subject2, int subject3, Date date) {
        return studentMapper.totalStudentByNextTimeExam(subject2,subject3,date);
    }

    @Override
    public List<Student> listStudentByReservationPeriod(int id, String date, int period) {
        return studentMapper.listStudentByReservationPeriod(id,date,period);
    }

    @Override
    public List<Student> listStudentByCid(int id, String type, Page page) {
        return "科目二".equals(type) ? studentMapper.listStudentByCid2(id,page) : studentMapper.listStudentByCid3(id,page);
    }

    @Override
    public int totalStudentByCid(int cid, String type) {
        return "科目二".equals(type) ? studentMapper.totalStudentByCid2(cid) : studentMapper.totalStudentByCid3(cid);
    }

    @Override
    public List<Student> listStudentByLastTimeExamInfo(int subject2, int subject3, Date date, Page page) {
        return studentMapper.listStudentByLastTimeExamInfo(subject2,subject3,date, page);
    }

    @Override
    public int totalStudentByLastTimeExamInfo(int subject2, int subject3, Date date) {
        return studentMapper.totalStudentByLastTimeExamInfo(subject2,subject3,date);
    }

    @Override
    public void update(Student student) {
        studentMapper.update(student);
    }

    @Override
    public Student getStudentById(int id) {
        return studentMapper.getStudentById(id);
    }


}
