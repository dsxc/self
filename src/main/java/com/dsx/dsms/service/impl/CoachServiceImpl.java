package com.dsx.dsms.service.impl;

import com.dsx.dsms.mapper.CoachMapper;
import com.dsx.dsms.pojo.Coach;
import com.dsx.dsms.pojo.ExamInfo;
import com.dsx.dsms.pojo.Scheduling;
import com.dsx.dsms.pojo.Student;
import com.dsx.dsms.service.CoachService;
import com.dsx.dsms.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author dsx
 */
@Service
public class CoachServiceImpl implements CoachService {

    @Autowired
    CoachMapper coachMapper;

    @Override
    public int add(Coach coach) {
        return coachMapper.add(coach);
    }

    @Override
    public List<Coach> list(Page page) {
        return coachMapper.list(page);
    }

    @Override
    public List<Coach> listCoachBySearch(String key, Page page) {
        return coachMapper.listCoachBySearch(key,page);
    }

    @Override
    public List<Coach> listAll() {
        return coachMapper.listAll();
    }

    @Override
    public void delete(int id) {
        coachMapper.delete(id);
    }

    @Override
    public int total() {
        return coachMapper.total();
    }

    @Override
    public List<Scheduling> listScheduling(int id, String date) {
        return coachMapper.listScheduling(id, date);
    }

    @Override
    public Scheduling getSchedulingById(int id, String date) {
        return coachMapper.getSchedulingById(id, date);
    }

    @Override
    public int getCountByPeriod(int id, String date, int period) {
        return coachMapper.getCountByPeriod(id, date, period);
    }

    @Override
    public void insertLeave(int id, String date, int period) {
        coachMapper.insertLeave(id,date,period);
    }
    @Override
    public void deleteLeave(int id, String date, int period) {
        coachMapper.deleteLeave(id,date,period);
    }

    @Override
    public int totalTime(int sid, int cid) {
        return coachMapper.totalTime(sid, cid);
    }

}
