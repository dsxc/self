package com.dsx.dsms.service.impl;

import com.dsx.dsms.mapper.ArticleMapper;
import com.dsx.dsms.pojo.Article;
import com.dsx.dsms.service.ArticleService;
import com.dsx.dsms.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author dsx
 */
@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    ArticleMapper articleMapper;
    @Override
    public void add(Article article) {
        articleMapper.add(article);
    }

    @Override
    public void delete(int id) {
        articleMapper.delete(id);
    }

    @Override
    public void update(Article article) {
        articleMapper.update(article);
    }

    @Override
    public List<Article> list(Page page) {
        return articleMapper.list(page);
    }

    @Override
    public List<Article> listArticleBySearch(Page page, String key) {
        return articleMapper.listArticleBySearch(page, key);
    }

    @Override
    public Article getArticleById(int id) {
        return articleMapper.getArticleById(id);
    }

    @Override
    public int total() {
        return articleMapper.total();
    }

}
