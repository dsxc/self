package com.dsx.dsms.service.impl;

import com.dsx.dsms.mapper.CommentMapper;
import com.dsx.dsms.pojo.Comment;
import com.dsx.dsms.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author dsx
 */
@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    CommentMapper commentMapper;

    @Override
    public void add(Comment comment) {
        commentMapper.add(comment);
    }

    @Override
    public void delete(int id) {
        commentMapper.delete(id);
    }

    @Override
    public List<Comment> listByCid(int cid) {
        return commentMapper.listByCid(cid);
    }


    @Override
    public Comment getCommentById(int id) {
        return commentMapper.getCommentById(id);
    }

    @Override
    public int total() {
        return commentMapper.total();
    }
}
