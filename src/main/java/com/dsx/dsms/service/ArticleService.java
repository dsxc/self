package com.dsx.dsms.service;

import com.dsx.dsms.pojo.Article;
import com.dsx.dsms.util.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author dsx
 */
public interface ArticleService {

    /**
     * 添加文章
     * @param article 文章
     */
    void add(Article article);

    /**
     * 删除文章
     * @param id 文章id
     */
    void delete(int id);

    /**
     * 更改文章
     * @param article 文章
     */
    void update(Article article);

    /**
     * 根据 page 获取文章列表
     * @param page 分页
     * @return 文章列表
     */
    List<Article> list(Page page);

    /**
     * 根据 page key 获取文章列表
     * @param page 分页
     * @param key 关键字
     * @return 文章列表
     */
    List<Article> listArticleBySearch(Page page, String key);

    /**
     * 通过id返回文章
     * @param id 文章id
     * @return 返回文章
     */
    Article getArticleById(int id);

    /**
     * 获取文章总数目
     * @return 文章总数目
     */
    int total();
}
