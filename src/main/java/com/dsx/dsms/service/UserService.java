package com.dsx.dsms.service;

import com.dsx.dsms.pojo.Student;
import com.dsx.dsms.pojo.User;
import org.apache.ibatis.annotations.Param;

/**
 * @author dsx
 */
public interface UserService {

    /**
     * 获取用户信息
     * @param username
     * @param password
     * @param type 用户类型
     * @return 用户
     */
    User getUserByType(String username, String password, int type);

    /**
     * 更新用户
     * @param user 用户
     */
    void upload(User user);

    /**
     * 删除用户
     * @param id 用户id
     */
    void delete(int id);


    /**
     * 返回用户
     * @param fid 外键id
     * @param type 用户类型
     * @return 用户
     */
    User getUserByFid(int fid, int type);



    int getUserByUserName(String username);
    int insertStudent(Student student);

    /**
     * 插入用户
     * @param username 用户名
     * @param password  密码
     * @param fid   外键
     * @param sex   性别
     * @param pic   图片名
     * @param userType 用户类型
     */
    void insertUser(String username, String password, int fid, String sex, String pic, int userType);

}
