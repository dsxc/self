package com.dsx.dsms.service;

import com.dsx.dsms.pojo.Coach;
import com.dsx.dsms.pojo.ExamInfo;
import com.dsx.dsms.pojo.Scheduling;
import com.dsx.dsms.pojo.Student;
import com.dsx.dsms.util.Page;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author dsx
 */
public interface CoachService {

    /**
     * 添加教练
     * @param coach 教练
     * @return 返回插入教练的id
     */
    int add(Coach coach);

    /**
     * 根据分页返回教练列表
     * @param page 分页
     * @return 教练列表
     */
    List<Coach> list(Page page);

    /**
     * 返回搜索结果
     * @param key 搜索关键字
     * @param page 分页
     * @return 教练
     */
    List<Coach> listCoachBySearch(@Param("key") String key, @Param("page") Page page);



    /**
     * 返回所有教练
     * @return 所有教练
     */
    List<Coach> listAll();

    void delete(int id);
    int total();
    List<Scheduling> listScheduling(int id, String date);
    Scheduling getSchedulingById(int id, String date);
    int getCountByPeriod(int id, String date, int period);
    void insertLeave(int id, String date, int period);
    void deleteLeave(int id, String date, int period);


    /**
     * 获取学生练车时间
     *
     * @param sid 学生id
     * @param cid 教练id
     * @return 练车时间
     */
    int totalTime(int sid, int cid);

}
