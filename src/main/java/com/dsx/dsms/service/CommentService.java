package com.dsx.dsms.service;

import com.dsx.dsms.pojo.Comment;

import java.util.List;

/**
 * @author dsx
 */
public interface CommentService {

    /**
     * 添加评论
     * @param comment 评论
     */
    void add(Comment comment);

    /**
     * 删除评论
     * @param id 评论id
     */
    void delete(int id);


    /**
     * 获得教练评论列表
     * @param cid 教练id
     * @return 评论列表
     */
    List<Comment> listByCid(int cid);


    /**
     * 通过id返回评论
     * @param id 评论id
     * @return 返回评论
     */
    Comment getCommentById(int id);

    /**
     * 获取评论总数目
     * @return 评论总数目
     */
    int total();
}
