package com.dsx.dsms.service;

import com.dsx.dsms.pojo.ExamInfo;
import com.dsx.dsms.util.Page;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author dsx
 */
public interface ExamInfoService {
    /**
     * 添加考试
     * @param examInfo 文章
     */
    void add(ExamInfo examInfo);

    /**
     * 删除考试
     * @param id 考试id
     */
    void delete(int id);


    /**
     * 更改考试
     * @param examInfo 考试
     */
    void update(ExamInfo examInfo);

    /**
     * 设定学生的考试结果状态
     * @param eid 考试信息
     * @param status 考试状态
     */
    void updateStudentExamStatus(int eid,int status);

    /**
     * 根据 page 获取考试列表
     * @param page 分页
     * @return 考试列表
     */
    List<ExamInfo> list(Page page);


    /**
     * 通过考试信息id返回考试信息
     * @param id 学生报名考试信息id
     * @return 返回考试信息
     */
    ExamInfo getExamInfoById(int id);

    /**
     * 获取考试总数目
     * @return 考试总数目
     */
    int total();


    /**
     * 根据学生id 返回他最近一次考试的信息
     * @param id 学生id
     * @return 返回未来最近一次考试的信息考试信息
     */
    ExamInfo getRecentlyExam(int id);

    /**
     *返回未来考试信息列表
     * @param date 当前日期
     * @return 返回考试信息
     */
    List<ExamInfo> listExamInfoByDate(Date date);


    /**
     * 插入学员考试记录
     * @param ef 考试信息
     * @param id 报名考试的学员id
     */
    void insertStudentExamInfo(ExamInfo ef, int id);


    /**
     * 学员取消考试
     * @param date 考试日期
     * @param sid  考试学员id
     */
    void cancelExam(@Param(value = "date") Date date, @Param(value = "sid") int sid);

    /**
     * 返回学员下一次考试的信息
     * @param sid 学员id
     * @param date 当前日期
     * @return 考试信息
     */
    ExamInfo getExamInfoByNextTime(int sid, Date date);

    /**
     * 返回type型考试的上一次考试信息
     * @param date 当前日期
     * @param type 考试类型
     * @return 上一次考试的时间
     */
    ExamInfo lastTimeExamInfo(Date date, int type);
}
