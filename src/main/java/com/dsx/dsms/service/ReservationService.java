package com.dsx.dsms.service;



public interface ReservationService {

    void updateReservation(String period,int num, int cid, String date);
    void insertReservation(int sid, int cid, String date, int period, int subject);
    void deleteReservation(int sid, String date, int period);
    int isReservation(int sid, String date, int period);
    int isLeave(int sid, String date, int period);
    int ReservationTime(int sid, String date);
    void resetTime(int cid, String date, int[] time);
}
