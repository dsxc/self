package com.dsx.dsms.util;

public class Common {

    /**
     * 两小时 milliseconds
     */
    public static final long TWO_HOUR = 7200000;
    /**
     * 8小时 milliseconds
     */
    public static final long EIGHT_HOUR = 28800000;
    /**
     * 一个月 milliseconds
     */
    public static final long ONE_MOUTH = 2592000000L;

    public static final String SUBJECT2 = "科目二";
    public static final String SUBJECT3 = "科目三";
}
