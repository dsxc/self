package com.dsx.dsms.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Transform {

    public String change(int num){
        String temp;
        switch (num){
            case 1:
                temp="科目一";
                break;
            case 2:
                temp="科目二";
                break;
            case 3:
                temp="科目三";
                break;
            case 4:
                temp="科目四";
                break;
            case 5:
                temp="完成";
                break;
            case 6:
                temp="拿到驾照";
                break;
            default:
                temp=null;
        }
        return temp;
    }
    public int change(String type){
        int temp=0;
        switch (type){
            case "科目一":
                temp=1;
                break;
            case "科目二":
                temp=2;
                break;
            case "科目三":
                temp=3;
                break;
            case "科目四":
                temp=4;
                break;
            case "完成所有科目":
                temp=5;
                break;
            case "拿到驾照":
                temp=6;
                break;
            default: break;
        }
        return temp;
    }



    public static String change(Date date){
        int num = date.getDay();
        String str;
        switch (num){
            case 1: str="周一"; break;
            case 2: str="周二"; break;
            case 3: str="周三"; break;
            case 4: str="周四"; break;
            case 5: str="周五"; break;
            case 6: str="周六"; break;
            default: str="周日"; break;

        }
        str = (date.getMonth()+1) + "月" + date.getDate() + "日 " + str;
        return str;
    }
    public String changeDate(Date d){
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDate = sd.format(d);


        return strDate;
    }
    public static String getRandomFileName() {

        SimpleDateFormat simpleDateFormat;

        simpleDateFormat = new SimpleDateFormat("yyyyMMdd");

        Date date = new Date();

        String str = simpleDateFormat.format(date);

        Random random = new Random();
        // 获取5位随机数
        int rannum = (int) (random.nextDouble() * (99999 - 10000 + 1)) + 10000;
        // 当前时间
        return str + rannum;
    }

}
