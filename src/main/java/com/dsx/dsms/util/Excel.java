package com.dsx.dsms.util;

import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.read.context.AnalysisContext;
import com.alibaba.excel.read.event.AnalysisEventListener;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.dsx.dsms.pojo.Student;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Excel {

    public List<Student> noModelMultipleSheet(InputStream in) {

        List<Student> list= new ArrayList<>();
        InputStream inputStream = in;
        try {
            ExcelReader reader = new ExcelReader(inputStream, ExcelTypeEnum.XLSX, null,
                    new AnalysisEventListener<List<String>>() {
                        @Override
                        public void invoke(List<String> object, AnalysisContext context) {
                            if(context.getCurrentRowNum() > 0){
                                Student student = new Student();
                                student.setName(object.get(0));
                                student.setTelephone(object.get(1));
                                student.setIdentity(object.get(2));
                                student.setAddress(object.get(3));
                                student.setSchedule(object.get(4));
                                list.add(student);
                            }

                            System.out.println(
                                    "当前sheet:" + context.getCurrentSheet().getSheetNo() + " 当前行：" + context.getCurrentRowNum()
                                            + " data:" + object);
                        }
                        @Override
                        public void doAfterAllAnalysed(AnalysisContext context) {

                        }
                    });
            reader.read();
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return list;
    }
}
