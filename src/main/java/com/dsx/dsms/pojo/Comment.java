package com.dsx.dsms.pojo;

import com.dsx.dsms.util.Transform;

import java.util.Date;

/**
 * @author dsx
 */
public class Comment {

    private int id;
    private int sid;
    private int cid;
    private String content;
    private Date time;
    private String timeStr;
    private Coach coach;
    private Student student;
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", sid=" + sid +
                ", cid=" + cid +
                ", content='" + content + '\'' +
                ", time=" + time +
                ", timeStr='" + timeStr + '\'' +
                ", coach=" + coach +
                ", student=" + student +
                '}';
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getTimeStr() {
        return timeStr;
    }

    public void setTimeStr(String timeStr) {
        this.timeStr = timeStr;
    }

    public Coach getCoach() {
        return coach;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
    }


    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
        Transform transform = new Transform();
        this.timeStr = transform.changeDate(time);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
