package com.dsx.dsms.pojo;

public class Student {
    private int id;
    private String name;
    private String schedule;
    private int subject2;
    private int subject3;
    private String telephone;
    private Coach coach2;
    private Coach coach3;
    private User user;
    private String identity;
    private String address;
    private int totalTime2;
    private int totalTime3;
    private String carModel;
    private ExamInfo examInfo;


    public ExamInfo getExamInfo() {
        return examInfo;
    }

    public void setExamInfo(ExamInfo examInfo) {
        this.examInfo = examInfo;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public int getTotalTime2() {
        return totalTime2;
    }

    public void setTotalTime2(int totalTime2) {
        this.totalTime2 = totalTime2;
    }

    public int getTotalTime3() {
        return totalTime3;
    }

    public void setTotalTime3(int totalTime3) {
        this.totalTime3 = totalTime3;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public int getSubject2() {
        return subject2;
    }

    public void setSubject2(int subject2) {
        this.subject2 = subject2;
    }

    public int getSubject3() {
        return subject3;
    }

    public void setSubject3(int subject3) {
        this.subject3 = subject3;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Coach getCoach2() {
        return coach2;
    }

    public void setCoach2(Coach coach2) {
        this.coach2 = coach2;
    }

    public Coach getCoach3() {
        return coach3;
    }

    public void setCoach3(Coach coach3) {
        this.coach3 = coach3;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", schedule='" + schedule + '\'' +
                ", subject2=" + subject2 +
                ", subject3=" + subject3 +
                ", telephone='" + telephone + '\'' +
                ", coach2=" + coach2 +
                ", coach3=" + coach3 +
                ", user=" + user +
                ", identity='" + identity + '\'' +
                ", address='" + address + '\'' +
                ", totalTime2=" + totalTime2 +
                ", totalTime3=" + totalTime3 +
                ", carModel='" + carModel + '\'' +
                ", examInfo=" + examInfo +
                '}';
    }
}
