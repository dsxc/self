package com.dsx.dsms.pojo;

import java.util.Date;

public class Scheduling {
    private int id;
    private int cid;
    private Date date;
    private int time1;
    private int time2;
    private int time3;
    private int time4;
    private int time5;
    private int time6;
    private int time7;
    private int time8;
    private String week;
    int[] time = new int[8];                      //每天8个时间段的预约人数
    int[] leave = new int[8];

    private boolean[] reservation = new boolean[8];    // 每天的8个时间段  学员是否预约/ 教练(false)是(true)否在训练场

    public boolean[] getReservation() {
        return reservation;
    }

    public void setReservation(int num) {  // 什么时间段预约了
        reservation[num] = true;
    }

    public void setTime(){
        time[0] = time1;
        time[1] = time2;
        time[2] = time3;
        time[3] = time4;
        time[4] = time5;
        time[5] = time6;
        time[6] = time7;
        time[7] = time8;
    }

    public int[] getTime() {
        return time;
    }

    public void setTime(int[] time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Scheduling{" +
                "id=" + id +
                ", cid=" + cid +
                ", date=" + date +
                ", time1=" + time1 +
                ", time2=" + time2 +
                ", time3=" + time3 +
                ", time4=" + time4 +
                ", time5=" + time5 +
                ", time6=" + time6 +
                ", time7=" + time7 +
                ", time8=" + time8 +
                '}';
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getTime1() {
        return time1;
    }

    public void setTime1(int time1) {
        this.time1 = time1;
    }

    public int getTime2() {
        return time2;
    }

    public void setTime2(int time2) {
        this.time2 = time2;
    }

    public int getTime3() {
        return time3;
    }

    public void setTime3(int time3) {
        this.time3 = time3;
    }

    public int getTime4() {
        return time4;
    }

    public void setTime4(int time4) {
        this.time4 = time4;
    }

    public int getTime5() {
        return time5;
    }

    public void setTime5(int time5) {
        this.time5 = time5;
    }

    public int getTime6() {
        return time6;
    }

    public void setTime6(int time6) {
        this.time6 = time6;
    }

    public int getTime7() {
        return time7;
    }

    public void setTime7(int time7) {
        this.time7 = time7;
    }

    public int getTime8() {
        return time8;
    }

    public void setTime8(int time8) {
        this.time8 = time8;
    }
}
