package com.dsx.dsms.pojo;

import com.dsx.dsms.util.Transform;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author dsx
 */
public class ExamInfo {
    private int id;
    private Date date;
    private int type;
    private String carModel;
    private String place;
    private String comment;
    private int status;
    private String date_;
    private String type_;
    private int sid;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    private Student student;

    @Override
    public String toString() {
        return "ExamInfo{" +
                "id=" + id +
                ", date=" + date +
                ", type=" + type +
                ", carModel='" + carModel + '\'' +
                ", place='" + place + '\'' +
                ", comment='" + comment + '\'' +
                ", date_='" + date_ + '\'' +
                ", type_='" + type_ + '\'' +
                ", sid=" + sid +
                ", student=" + student +
                '}';
    }

    public String getDate_() {
        return date_;
    }

    public void setDate_() {
        this.date_ = new Transform().changeDate(date);
    }

    public String getType_() {
        return type_;
    }

    public void setType_() {
        this.type_ =  new Transform().change(type);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
