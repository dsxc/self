package com.dsx.dsms.pojo;

public class User {
    private int id;
    private String username;
    private String password;
    private Student student;
    private Coach coach;
    private String sex;
    private String pic;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public com.dsx.dsms.pojo.Coach getCoach() {
        return coach;
    }

    public void setCoach(com.dsx.dsms.pojo.Coach coach) {
        this.coach = coach;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", student=" + student +
                ", Coach=" + coach +
                '}';
    }
}
