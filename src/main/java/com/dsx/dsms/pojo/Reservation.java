package com.dsx.dsms.pojo;

import java.util.Date;

public class Reservation {
    private int id,sid,period,type;
    private Date date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "reservation{" +
                "id=" + id +
                ", sid=" + sid +
                ", period=" + period +
                ", type=" + type +
                ", date=" + date +
                '}';
    }
}
