package com.dsx.dsms.test;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class TestDsms {

    public static void main(String args[]) throws ParseException {
        //准备分类测试数据：
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try (Connection c =
                     DriverManager.getConnection("jdbc:mysql://localhost:3306/ds?useUnicode=true&characterEncoding=utf8",
                        "root", "admin")) {
            String sql = "insert into scheduling(cid, date) value(4,?)";
            PreparedStatement ps = c.prepareStatement(sql);
            Date date = new Date();
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            date.setDate(date.getDate()-40);
            System.out.println(sf.format(date));
            for(int i=0; i<120; i++){
                date.setDate(date.getDate()+1);
                java.sql.Date date1 = new java.sql.Date(date.getTime());
                ps.setDate(1,date1);
                ps.execute();
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
