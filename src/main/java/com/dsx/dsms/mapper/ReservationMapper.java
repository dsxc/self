package com.dsx.dsms.mapper;

import org.apache.ibatis.annotations.Param;


public interface ReservationMapper {
   void updateReservation(@Param(value = "period") String period,
                                  @Param(value = "num") int num,
                                  @Param(value = "cid") int cid,
                                  @Param(value = "date") String date);
   void insertReservation(@Param(value = "sid") int sid,
                          @Param(value = "cid") int cid,
                          @Param(value = "date") String date,
                          @Param(value = "period") int period,
                          @Param(value = "subject") int subject);

   void deleteReservation(@Param(value = "sid") int sid,
                          @Param(value = "date") String date,
                          @Param(value = "period") int period);
   int isReservation(@Param(value = "sid") int sid,
                      @Param(value = "date") String date,
                      @Param(value = "period") int period);
   int isLeave(@Param(value = "sid") int sid,
                     @Param(value = "date") String date,
                     @Param(value = "period") int period);
   int ReservationTime(@Param(value = "sid") int sid,
                     @Param(value = "date") String date);
   void resetTime(@Param(value = "cid") int cid,
                 @Param(value = "date") String date,
                 @Param(value = "time") int[] time);
}
