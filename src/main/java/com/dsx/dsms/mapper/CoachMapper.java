package com.dsx.dsms.mapper;

import com.dsx.dsms.pojo.Coach;
import com.dsx.dsms.pojo.ExamInfo;
import com.dsx.dsms.pojo.Scheduling;
import com.dsx.dsms.pojo.Student;
import com.dsx.dsms.util.Page;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface CoachMapper {


    /**
     *添加教练并返回此教练的id
     * @param coach 添加教练
     * @return 返回插入教练的主键值
     */
    int add(Coach coach);


    /**
     * 返回搜索结果
     * @param key 搜索关键字
     * @param page 分页
     * @return 教练
     */
    List<Coach> listCoachBySearch(@Param("key") String key, @Param("page") Page page);
    List<Coach> list(Page page);
    List<Coach> listAll();
    void delete(int id);
    int total();
    List<Scheduling> listScheduling(@Param(value="id") int id, @Param(value="date") String date);
    Scheduling getSchedulingById(@Param(value="id") int id, @Param(value="date") String date);
    int getCountByPeriod(@Param(value="id") int id, @Param(value="date") String date, @Param(value = "period") int period);
    void insertLeave(@Param(value="id") int id, @Param(value="date") String date, @Param(value = "period") int period);
    void deleteLeave(@Param(value="id") int id, @Param(value="date") String date, @Param(value = "period") int period);
    int totalTime(@Param(value="sid") int sid, @Param(value="cid") int cid);
}
