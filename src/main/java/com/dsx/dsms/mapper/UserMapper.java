package com.dsx.dsms.mapper;

import com.dsx.dsms.pojo.Student;
import com.dsx.dsms.pojo.User;
import org.apache.ibatis.annotations.Param;

/**
 * @author dsx
 */
public interface UserMapper {


    /**
     * 删除用户
     * @param id 用户id
     */
    void delete(int id);


    /**
     * 返回用户
     * @param fid 外键id
     * @param type 用户类型
     * @return 用户
     */
    User getUserByFid(@Param(value = "fid") int fid, @Param(value = "type") int type);


    User getManager(@Param(value = "username") String username, @Param(value="password") String password);
    User getCoach(@Param(value = "username") String username, @Param(value="password") String password);
    User getStudent(@Param(value = "username") String username, @Param(value="password") String password);
    int getUserByUserName(String username);
    int insertStudent(Student student);

    /**
     * 插入用户
     * @param username 用户名
     * @param password  密码
     * @param fid   外键
     * @param sex   性别
     * @param pic   图片名
     * @param userType 用户类型
     */
    void insertUser(@Param(value = "username") String username, @Param(value = "password") String password,
                    @Param(value = "fid") int fid, @Param(value = "sex") String sex, @Param(value = "pic") String pic,
                    @Param(value = "userType") int userType);

    /**
     * 更新用户
     * @param user 用户
     */
    void upload(User user);
}
