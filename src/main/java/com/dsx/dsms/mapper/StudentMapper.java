package com.dsx.dsms.mapper;

import com.dsx.dsms.pojo.ExamInfo;
import com.dsx.dsms.pojo.Student;
import com.dsx.dsms.util.Page;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author dsx
 */
public interface StudentMapper {



    /**
     *
     * 返回学生列表
     * @param page 分页
     * @return 返回学生列表
     */
    List<Student> list(Page page);
    /**
     * 根据进度返回学员
     * @param page 分页
     * @param schedule 学员进度
     * @return 学员
     */
    List<Student> listBySchedule(@Param(value = "page") Page page, @Param(value="schedule") String schedule);



    /**
     * 返回学生列表
     * @param page 分页
     * @param schedule 进度
     * @param key 搜索关键字
     * @return 学生
     */
    List<Student> listStudentBySearch(@Param(value = "page") Page page, @Param(value="schedule") String schedule, @Param("key") String key);


    int total(String schedule);
    void delete(int id);
    void add(Student student);

    /**
     * 返回下一次考试的学员  如果subject2 不为0 就返回考科目二的 否则就是科目三的
     * @param subject2 科目二教练id
     * @param subject3 科目三教练id
     * @param date  当前日期
     * @param page 分页
     * @return 学院列表
     */
    List<Student> listStudentByNextTimeExam(@Param(value = "subject2") int subject2, @Param(value = "subject3") int subject3,
                                            @Param(value = "date") Date date, @Param(value = "page") Page page);
    /**
     * 返回下一次考试的学员 人数
     * @param subject2 科目二教练id
     * @param subject3 科目三教练id
     * @param date  当前日期
     * @return 学院列表
     */
    int totalStudentByNextTimeExam(@Param(value = "subject2") int subject2, @Param(value = "subject3") int subject3, @Param(value = "date") Date date);

    /**
     * 在此时间段预约的时间
     * @param id 学生id
     * @param date 日期
     * @param period 时间段
     * @return 在此时间段预约的时间
     */
    List<Student> listStudentByReservationPeriod(
            @Param(value="id") int id, @Param(value="date")
            String date, @Param(value = "period") int period);

    /**
     * 此教练的学生
     * @param cid 教练id
     * @param page 分页
     * @return 此教练的学生
     */
    List<Student> listStudentByCid2(@Param(value="cid") int cid, @Param(value = "page") Page page);
    /**
     * 此教练的学生人数
     * @param cid 教练id
     * @return 此教练的学生人数
     */
    int totalStudentByCid2(@Param(value="cid") int cid);

    /**
     * 此教练的学生 科目二
     * @param cid 教练id
     * @param page 分页
     * @return 此教练的学生
     */
    List<Student> listStudentByCid3(@Param(value="cid") int cid, @Param(value = "page") Page page);
    /**
     * 此教练的学生人数 科目三
     * @param cid 教练id
     * @return 此教练的学生人数
     */
    int totalStudentByCid3(@Param(value="cid") int cid);


    /**
     * 获取上一次考试的学生列表
     * @param subject2  科目二教练id
     * @param subject3  科目三教练id
     * @param date  考试日期
     * @param page 分页
     * @return  学生列表
     */
    List<Student> listStudentByLastTimeExamInfo(@Param("subject2") int subject2, @Param("subject3") int subject3, @Param("date") Date date, @Param("page") Page page);
/**
     * 获取上一次考试的学生数
     * @param subject2  科目二教练id
     * @param subject3  科目三教练id
     * @param date  考试日期
     * @return  学生数
     */
    int totalStudentByLastTimeExamInfo(@Param("subject2") int subject2, @Param("subject3") int subject3, @Param("date") Date date);

    /**
     * 更新
     * @param student 学生
     */
    void update(Student student);


    /**
     * 根据id 返回学生
     * @param id 学生id
     * @return 返回学生
     */
    Student getStudentById(int id);

}
