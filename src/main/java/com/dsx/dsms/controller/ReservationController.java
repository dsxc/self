package com.dsx.dsms.controller;


import com.alibaba.fastjson.JSONObject;
import com.dsx.dsms.pojo.User;
import com.dsx.dsms.service.ReservationService;
import com.dsx.dsms.util.Common;
import org.hamcrest.core.Is;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("")
public class ReservationController {

    @Autowired
    ReservationService service;

    @ResponseBody
    @RequestMapping("fore_student_reservation")
    public String reservation(int j, long t, int num, int flag, HttpSession session){
                   //第几列(从0开始) 的第几天   num 预约人数  flag 是否预约 1 -1
        User user = (User)session.getAttribute("student");
        Date date = new Date(t);
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = sd.format(date);
        int sid = user.getStudent().getId();
        int Is = service.isReservation(sid,strDate,j);
        JSONObject jsonObject = new JSONObject();
        int subject,cid;
        String str = "time" + (j+1);
        int reservationTime =  service.ReservationTime(sid,strDate);

        // 设置 练习科目
        if(user.getStudent().getSubject3() != 0){
            subject = 3;
            cid = user.getStudent().getSubject3();
        }else{
            subject = 2;
            cid = user.getStudent().getSubject2();
        }
        System.out.println("Is-----> " + Is + "flag--------->  " + flag);
        //选择的时间
        Date d1 = new Date(t);
        d1.setHours(8+j);
        //当前的时间
        Date d2 = new Date();
        if(Is == 0) {

            if(flag == -1){
                //-11 您目前还没有预约
                jsonObject.put("success",-11);
            }
            else{
                if(reservationTime >= 4){
                    //33 预约总时长大于4小时
                    jsonObject.put("success", 33);
                }
                else {
                    //只能提前两小时预约
                    if(d1.getTime() - d2.getTime() < Common.TWO_HOUR){
                        jsonObject.put("success", 99);
                    } //只能预约最近一个月的
                    else if(d1.getTime() - d2.getTime() > Common.ONE_MOUTH){
                        jsonObject.put("success", 111);
                    }
                    else{
                        service.insertReservation(sid,cid,strDate,j,subject);
                        service.updateReservation(str,num,cid,strDate);
                        // 成功预约
                        jsonObject.put("success", 1);
                    }
                }
            }
        }else if(Is > 0){
            if(flag == 1){
                //111 您已经预约
                jsonObject.put("success",11);
            }else{
                //只能提前两小时取消预约
                if(d1.getTime() - d2.getTime() < Common.TWO_HOUR){
                    jsonObject.put("success", -99);
                }
                else{
                    service.deleteReservation(sid,strDate,j);
                    service.updateReservation(str,num,cid,strDate);
                    //成功取消预约
                    jsonObject.put("success", -1);
                }
            }
        }
        return jsonObject.toJSONString();
    }

}
