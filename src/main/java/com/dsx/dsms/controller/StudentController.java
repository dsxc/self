package com.dsx.dsms.controller;


import com.alibaba.fastjson.JSONObject;
import com.dsx.dsms.pojo.*;
import com.dsx.dsms.service.CoachService;
import com.dsx.dsms.service.CommentService;
import com.dsx.dsms.service.ExamInfoService;
import com.dsx.dsms.service.StudentService;
import com.dsx.dsms.util.Excel;
import com.dsx.dsms.util.Page;
import com.dsx.dsms.util.Transform;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * @author dsx
 */
@Controller
@RequestMapping("")
public class StudentController {
    @Autowired
    StudentService studentService;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    ExamInfoService examInfoService;
    @Autowired
    CommentService commentService;
    @Autowired
    CoachService coachService;

    @ResponseBody
    @RequestMapping("admin_student_list")
    public String listBySchedule(Page page, Integer schedule){
        if(schedule == null){
            schedule=0;
        }
        List<Student> ss =studentService.listBySchedule(page,schedule);
        int total = studentService.total(schedule);
        page.setTotal(total);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ss", ss);
        jsonObject.put("page", page);
        System.out.println(jsonObject.toJSONString());
        return jsonObject.toJSONString();
    }
    @ResponseBody
    @RequestMapping("admin_student_delete")
    public void delete(int id){

        System.out.println("address ---> " + request.getServletPath());
        studentService.delete(id);
    }


    @RequestMapping("/uploadStudent")
    public ModelAndView upload(HttpServletRequest request, MultipartFile excel, String remarks )
            throws IllegalStateException, IOException {

        Excel tool = new Excel();
        List<Student> ss = tool.noModelMultipleSheet(excel.getInputStream());
        for(Student s : ss){
            studentService.add(s);
        }
        System.out.println(ss);
        ModelAndView mav = new ModelAndView("redirect:/listStudent.html");
        mav.addObject("schedule",1);
        return mav;
    }

    @ResponseBody
    @RequestMapping("fore_student_listExam")
    public String listExam(HttpSession session){

        int flag = 1;
        User user = (User)session.getAttribute("student");
        int id = user.getStudent().getId();
        ExamInfo examInfo = examInfoService.getRecentlyExam(id);
        Date date = new Date();
        JSONObject jsonObject = new JSONObject();
        if( examInfo!=null && ( date.getTime() - examInfo.getDate().getTime() < 0)) {
            //已经报名未来几个月内的考试
            flag = 2;
            jsonObject.put("flag", flag);
            return jsonObject.toJSONString();
        }
        List<ExamInfo> es = examInfoService.listExamInfoByDate(date);
        for(ExamInfo e : es){
            e.setDate_();
            e.setType_();
        }
        System.out.println(es);
        jsonObject.put("es", es);
        jsonObject.put("flag", flag);
        return jsonObject.toJSONString();
    }

    @ResponseBody
    @RequestMapping("fore_student_RecentlyExam")
    public String recentlyExamInfo(HttpSession session){

        User user = (User)session.getAttribute("student");
        int id = user.getStudent().getId();
        ExamInfo examInfo = examInfoService.getRecentlyExam(id);
        examInfo.setType_();
        examInfo.setDate_();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("e", examInfo);
        return jsonObject.toJSONString();
    }

    @ResponseBody
    @RequestMapping("fore_student_examSinUp")
    public String examSinUp(int id, HttpSession session){
        int flag=0;
        User user = (User)session.getAttribute("student");
        int uid = user.getId();
        ExamInfo examInfo = examInfoService.getExamInfoById(id);
        int examType = examInfo.getType();

        int userType = new Transform().change(user.getStudent().getSchedule());
        JSONObject jsonObject = new JSONObject();
        if(examType!=userType){
            //考试类型不匹配
            flag = 1;
        }
        else {
            examInfoService.insertStudentExamInfo(examInfo,uid);
        }
        jsonObject.put("flag", flag);
        jsonObject.put("info", user.getStudent().getSchedule());
        return jsonObject.toJSONString();
    }
    @ResponseBody
    @RequestMapping("fore_student_cancelExam")
    public String cancelExam(HttpSession session){
        User user = (User)session.getAttribute("student");
        int id = user.getStudent().getId();
        ExamInfo examInfo = examInfoService.getRecentlyExam(id);
        examInfoService.cancelExam(examInfo.getDate(),id);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("flag", 1);
        return jsonObject.toJSONString();
    }

    @RequestMapping("addComment")
    public String addComment(Comment comment, HttpSession session){
        User user = (User) session.getAttribute("student");
        Student student = user.getStudent();
        int cid = comment.getCid();
        if(cid != student.getSubject2() && cid != student.getSubject3()){
            return "redirect:comment.html?cid=" + comment.getCid() + "&msg=-1";
        }
        comment.setSid(student.getId());
        comment.setTime(new Date());
        commentService.add(comment);
        return "redirect:comment.html?cid=" + comment.getCid();
    }

    @ResponseBody
    @RequestMapping("fore_student_listCommentByCid")
    public String  listCommentByCid(int cid){
        List<Comment> comments = commentService.listByCid(cid);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("cs", comments);
        jsonObject.put("cid", cid);
        return jsonObject.toJSONString();
    }

}
