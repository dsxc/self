package com.dsx.dsms.controller;


import com.alibaba.fastjson.JSONObject;
import com.dsx.dsms.pojo.Article;
import com.dsx.dsms.pojo.Coach;
import com.dsx.dsms.pojo.ExamInfo;
import com.dsx.dsms.pojo.Student;
import com.dsx.dsms.service.ArticleService;
import com.dsx.dsms.service.CoachService;
import com.dsx.dsms.service.ExamInfoService;
import com.dsx.dsms.service.StudentService;
import com.dsx.dsms.util.Page;
import com.dsx.dsms.util.Transform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author dsx
 */
@Controller
@RequestMapping("")
public class AdminController {
    @Autowired
    ArticleService articleService;
    @Autowired
    ExamInfoService examInfoService;
    @Autowired
    CoachService coachService;
    @Autowired
    StudentService studentService;

    @RequestMapping("admin_article_add")
    public String articleAdd(Article article){
        Date date = new Date();
        article.setTime(date);
        articleService.add(article);
        return "redirect:admin/listArticle.html";
    }

    @ResponseBody
    @RequestMapping("admin_article_delete")
    public void articleDelete(int id){
        articleService.delete(id);
    }

    @RequestMapping("admin_article_update")
    public String articleUpdate(Article article){
        article.setTime(new Date());
        articleService.update(article);
        return "redirect:admin/listArticle.html";
    }

    @ResponseBody
    @RequestMapping("admin_article_list")
    public String articleList(Page page){
        List<Article> as =articleService.list(page);
        Transform transform = new Transform();
        for(Article a : as){
            a.setTimeStr(transform.changeDate(a.getTime()));
        }
        int total = articleService.total();
        page.setTotal(total);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("as", as);
        jsonObject.put("page", page);
        System.out.println(jsonObject.toJSONString());
        return jsonObject.toJSONString();
    }

    @ResponseBody
    @RequestMapping("admin_article_get")
    public String getArticleById(int id){
        Article article = articleService.getArticleById(id);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("article", article);
        return jsonObject.toJSONString();
    }


    @RequestMapping("admin_examInfo_add")
    public String examInfoAdd(String dateStr, ExamInfo examInfo) throws ParseException {
        dateStr = dateStr.replace('T', ' ');
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        examInfo.setDate(sf.parse(dateStr));
        examInfoService.add(examInfo);
        return "redirect:admin/listExamInfo.html";
    }

    @ResponseBody
    @RequestMapping("admin_examInfo_delete")
    public void examInfoDelete(int id){
        examInfoService.delete(id);
    }

    @RequestMapping("admin_examInfo_update")
    public String examInfoUpdate(String dateStr, ExamInfo examInfo) throws ParseException {
        dateStr = dateStr.replace('T', ' ');
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        examInfo.setDate(sf.parse(dateStr));
        examInfoService.update(examInfo);
        return "redirect:admin/listExamInfo.html";
    }

    @ResponseBody
    @RequestMapping("admin_examInfo_list")
    public String examInfoList(Page page){
        List<ExamInfo> es =examInfoService.list(page);
        for(ExamInfo e : es){
            e.setDate_();
            e.setType_();
        }
        int total = examInfoService.total();
        page.setTotal(total);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("es", es);
        jsonObject.put("page", page);
        System.out.println(jsonObject.toJSONString());
        return jsonObject.toJSONString();
    }

    @ResponseBody
    @RequestMapping("admin_examInfo_get")
    public String getExamInfoById(int id){
        ExamInfo examInfo = examInfoService.getExamInfoById(id);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("examInfo", examInfo);
        return jsonObject.toJSONString();
    }

    @ResponseBody
    @RequestMapping("admin_search")
    public String search(Page page, String key, Integer schedule, String type){

        JSONObject jsonObject = new JSONObject();
        switch (type){
            case "教练":
                List<Coach> cs = coachService.listCoachBySearch(key,page);
                jsonObject.put("cs", cs);
                break;
            case "学员":
                List<Student> ss = studentService.listStudentBySearch(page,new Transform().change(schedule),key);
                jsonObject.put("ss", ss);
                break;
            case "文章":
                List<Article> as = articleService.listArticleBySearch(page,key);
                jsonObject.put("as", as);
                break;
            default: break;
        }
        jsonObject.put("page", page);
        return jsonObject.toJSONString();
    }








}
